# sdp-canonical_ubuntu_16.04_lts_stig_baseline-overlay

InSpec profile overlay to validate secure configuration of Waverly Labs' SDP Gateway server against [DISA](https://iase.disa.mil/stigs/)'s Canonical Ubuntu 16.04 LTS Security Technical Implementation Guide (STIG) Version 1 Release 1.

## Getting Started  
It is intended and recommended that InSpec run this profile from a __"runner"__ host (such as a DevOps orchestration server, an administrative management system, or a developer's workstation/laptop) against the target (e.g. SDP Gateway server) remotely over __ssh__.

__For the best security of the runner, always install on the runner the _latest version_ of InSpec and supporting Ruby language components.__ 

Latest versions and installation options are available at the [InSpec](http://inspec.io/) site.

## Tailoring to Your Environment
The following inputs may be configured in an inputs ".yml" file for the profile to run correctly for your specific environment. More information about InSpec inputs can be found in the [InSpec Profile Documentation](https://www.inspec.io/docs/reference/profiles/).

```yaml
- name: admin_usernames
  description: List of admin users the SDP gateway server
  type: Array
  value: []

#file-access
- name: gw_admin_files
  description: List of admin files and their permissions on the SDP gateway server
  type: Array
  value: []

#gw-port-check
- name: gw_listening_processes
  description: List of process/port pairs listening on the SDP gateway server
  type: Array
  value: []

#iptables-rules
- name: gw_iptables_rules
  description: "iptables rules that should exist."
  type: Array
  value: []

#V-75545
- name: known_system_accounts
  description: System accounts that support approved system activities
  type: Array
  value: []
  profile: canonical-ubuntu-16.04-lts-stig-baseline

#V-75545
- name: disallowed_accounts
  description: Accounts that are not allowed on the system
  type: Array
  value: []
  profile: canonical-ubuntu-16.04-lts-stig-baseline

#V-75545
- name: user_accounts
  description: Accounts of known managed users
  type: Array
  value: []
  profile: canonical-ubuntu-16.04-lts-stig-baseline

#V-75469
- name: emergency_accounts
  description: Emergency user accounts
  type: Array
  value: []
  profile: canonical-ubuntu-16.04-lts-stig-baseline

#V-75485
- name: max_account_inactive_days
  description: Maximum number of days an account can remain inactive
  type: Numeric
  value:
  profile: canonical-ubuntu-16.04-lts-stig-baseline

#V-75435, V-75825
- name: banner_text
  description: Standard Mandatory DoD Notice and Consent Banner
  type: String
  value: ''
  profile: canonical-ubuntu-16.04-lts-stig-baseline

#V-75443
- name: maxlogins
  description: Maximum number of concurrent sessions
  type: Numeric
  value:
  profile: canonical-ubuntu-16.04-lts-stig-baseline

#V-75837
- name: client_alive_interval
  description: Client Alive Interval
  type: Numeric
  value:
  profile: canonical-ubuntu-16.04-lts-stig-baseline

#V-75837
- name: client_alive_count_max
  description: Client Alive Interval
  type: Numeric
  value:
  profile: canonical-ubuntu-16.04-lts-stig-baseline

#V-75867
- name: allowed_network_interfaces
  description: Array of allowed network interfaces (wired & wireless)
  type: Array
  value: []
  profile: canonical-ubuntu-16.04-lts-stig-baseline

#V-75449
- name: min_num_uppercase_char
  description: Minimum number of upper case characters needed in a password (Denoted in negative numbers)
  type: Numeric
  value: 
  profile: canonical-ubuntu-16.04-lts-stig-baseline

#V-75451
- name: min_num_lowercase_char
  description: Minimum number of lower case characters needed in a password (Denoted in negative numbers)
  type: Numeric
  value: 
  profile: canonical-ubuntu-16.04-lts-stig-baseline

#V-75453
- name: min_num_numeric_char
  description: Minimum number of numeric characters needed in a password (Denoted in negative numbers)
  type: Numeric
  value: 
  profile: canonical-ubuntu-16.04-lts-stig-baseline

#V-75455
- name: min_num_special_char
  description: Minimum number of special characters needed in a password (Denoted in negative numbers)
  type: Numeric
  value: 
  profile: canonical-ubuntu-16.04-lts-stig-baseline

#V-75457
- name: min_num_characters_to_change
  description: Minimum number of characters that need to be changed for password rotation
  type: Numeric
  value: 
  profile: canonical-ubuntu-16.04-lts-stig-baseline
```

# Running This Overlay Directly from GitLab

```
# How to run
inspec exec https://gitlab.mitre.org/jweiss/sdp-overlay/-/archive/master/sdp-overlay-master.tar.gz -t ssh://<hostname>:<port> --sudo --input-file=<path_to_your_inputs_file/name_of_your_inputs_file.yml> --reporter=cli json:<path_to_your_output_file/name_of_your_output_file.json>
```

### Different Run Options

  [Full exec options](https://docs.chef.io/inspec/cli/#options-3)

## Running This Overlay from a local Archive copy 

If your runner is not always expected to have direct access to GitHub and/or Gitlab, use the following steps to create an archive bundle of this overlay and all of its dependent tests:

(Git is required to clone the InSpec profile using the instructions below. Git can be downloaded from the [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) site.)

When the __"runner"__ host uses this profile overlay for the first time, follow these steps: 

```
mkdir profiles
cd profiles
git clone  git@gitlab.mitre.org:jweiss/sdp-overlay.git
inspec archive sdp-overlay
inspec exec <name of generated archive> -t ssh://<hostname>:<port> --sudo --input-file=<path_to_your_inputs_file/name_of_your_inputs_file.yml> --reporter=cli json:<path_to_your_output_file/name_of_your_output_file.json>
```
For every successive run, follow these steps to always have the latest version of this overlay:

```
cd sdp-overlay
git pull
cd ..
inspec archive sdp-overlay --overwrite
inspec exec <name of generated archive> -t ssh://<hostname>:<port> --sudo --input-file=<path_to_your_inputs_file/name_of_your_inputs_file.yml> --reporter=cli json:<path_to_your_output_file/name_of_your_output_file.json>
```

## Viewing the JSON Results

The JSON results output file can be loaded into __[heimdall-lite](https://heimdall-lite.mitre.org/)__ for a user-interactive, graphical view of the InSpec results. 

The JSON InSpec results file may also be loaded into a __[full heimdall server](https://github.com/mitre/heimdall)__, allowing for additional functionality such as to store and compare multiple profile runs.

## Authors
* Daniel Tingstrom
* Joshua Weiss

## Special Thanks
* Eugene Aronne
* Robert Clark

## Contributing and Getting Help
To report a bug or feature request, please open an [issue](https://gitlab.com/atarc/orion/-/issues).

### NOTICE

© 2018-2021 The MITRE Corporation.

Approved for Public Release; Distribution Unlimited. Case Number 18-3678.

### NOTICE

MITRE hereby grants express written permission to use, reproduce, distribute, modify, and otherwise leverage this software to the extent permitted by the licensed terms provided in the LICENSE.md file included with this project.

### NOTICE

DISA STIGs are published by DISA IASE, see: https://iase.disa.mil/Pages/privacy_policy.aspx.


