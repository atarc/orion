control 'admins-check' do
  title "The SDP gateway server should be configured to have a small set of specific admin users"
  tag nist: ["AC-2 (7)"]

  describe groups.where { name == 'wheel' } do
    it { should exist }
    its('members') { should eq input('admin_usernames') }
  end
end

control 'file-access' do
  title "The SDP gateway server should be configured to store specific files with admin ownership and permissions"
  tag nist: ["AC-3"]

  #given a list of [file name, file owner, file permission] tuples, verify these files match permissions
  files_to_check = input("gw_admin_files")
  files_to_check.each do |(file_name, file_owner, file_perm)|
    describe file(file_name) do
      it { should be_owned_by file_owner }
      its('mode') { should cmp file_perm }
    end
  end
end

control 'gw-port-check' do
  title "The SDP gateway server should be configured to enable network connectivity to clients and the controller"
  tag nist: ["AC-4"]

  #given a list of [process name, port #] pairs, ensure they are in the list of network connections
  procs_listening = input("gw_listening_processes")
  procs_listening.each do |(proc_name, port_num)|
    describe port(port_num) do
      it { should be_listening }
      its('processes') { should include proc_name }
    end
  end

end

control 'iptables-rules' do
  title "The SDP gateway server should be configured to accept network connections on specific network ports"
  tag nist: ["AC-17"]

  #check if iptables service is enabled and running
  describe service('iptables') do
    it { should be_enabled }
    it { should be_running }
  end

  #verify all iptables rules specified by inputs exist
  iptables_rules = input("gw_iptables_rules")
  describe iptables do
    iptables_rules.each do |rule|
      it { should have_rule(rule) }
    end
  end

end

require_controls 'canonical-ubuntu-16.04-lts-stig-baseline' do

  #checks system for known system accounts, disallowed accounts
  #inputs: known_system_accounts, disallowed_accounts, user_accounts
  control 'V-75545' do
    tag nist: ["AC-2 (9)"]
  end

  #check emergency accounts
  control 'V-75469' do
    tag nist: ["AC-2 (02)"]
  end

  #check temporary accounts
  control 'V-75491' do
    tag nist: ["AC-2 (02)"]
  end
  
  #checks for duplicate user IDs
  control 'V-75547' do
    tag nist: ["AC-2"]
  end

  #account disabled after long inactivity period
  #inputs: max_account_inactive_days
  control 'V-75485' do
    tag nist: ["IA-4"]
  end

  #--- start of auditing controls

  #auditd is running
  control 'V-80959' do
    tag nist: ["AC-2 (04)"]
  end
  
  #audit changes to /etc/passwd
  control 'V-75661' do
    tag nist: ["AC-2 (04)"]
  end

  #audit changes to /etc/group
  control 'V-75663' do
    tag nist: ["AC-2 (04)"]
  end

  #audit changes to /etc/gshadow
  control 'V-75665' do
    tag nist: ["AC-2 (04)"]
  end

  #audit changes to /etc/shadow
  control 'V-75667' do
    tag nist: ["AC-2 (04)"]
  end

  #audit changes to /etc/security/opasswd
  control 'V-75687' do
    tag nist: ["AC-2 (04)"]
  end

  #audit usage of su command
  control 'V-75691' do
    tag nist: ["AC-2 (04)"]
  end

  #audit usage of sudo command
  control 'V-75755' do
    tag nist: ["AC-2 (04)", "AC-6 (09)"]
  end

  #audit usage of sudoedit command
  control 'V-75757' do
    tag nist: ["AC-2 (04)"]
  end

  #audit usage of usermod command
  control 'V-75785' do
    tag nist: ["AC-2 (04)"]
  end

  #--- end of auditing controls

  #auto logout after X mins of inactivity
  control 'V-75441' do
    tag nist: ["AC-2 (05)"]
  end
  
  #automatically lock account after 3 attempts
  control 'V-75487' do
    tag nist: ["AC-7"]
  end

  #enforce delay of 4 secs between logon prompts following a failed logon attempt
  control 'V-75493' do
    tag nist: ["AC-7"]
  end

  #banner checks
  #banner must be enabled
  control 'V-75393' do
    tag nist: ["AC-8"]
  end

  #banner must installed on OS
  #input: banner_text
  control 'V-75435' do
    tag nist: ["AC-8"]
  end

  #banner must be installed for SSH logins
  #input: banner_text
  control 'V-75825' do
    tag nist: ["AC-8"]
  end

  #concurrent sessions
  #input: maxlogins
  control 'V-75443' do
    tag nist: ["AC-10"]
  end
  
  #inactivity during SSH sessions
  #inputs: client_alive_interval, client_alive_count_max
  control 'V-75837' do
    tag nist: ["AC-12"]
  end

  #check for users with no password set
  control 'V-75479' do
    tag nist: ["AC-14"]
  end

  #checks if firewall package 'ufw' installed
  control 'V-75803' do
    tag nist: ["AC-17 (01)"]
  end

  #checks if ufw is enabled
  control 'V-75805' do
    tag nist: ["AC-17 (01)"]
  end

  #SSH key strength for confidentiality
  control 'V-75829' do
    tag nist: ["AC-17 (02)"]
  end

  #SSH key strength for integrity
  control 'V-75831' do
    tag nist: ["AC-17 (02)"]
  end

  #check for wireless interface
  #input: allowed_network_interfaces
  control 'V-75867' do
    tag nist: ["AC-18"]
  end

  #check for automounting of USB devices
  control 'V-75531' do
    tag nist: ["AC-20 (02)"]
  end

  #--- start of password checking controls

  #input: min_num_uppercase_char
  control 'V-75449' do
    tag nist: ["IA-5 (01)"]
  end

  #input: min_num_lowercase_char
  control 'V-75451' do
    tag nist: ["IA-5 (01)"]
  end

  #input: min_num_numeric_char
  control 'V-75453' do
    tag nist: ["IA-5 (01)"]
  end

  #input: min_num_special_char
  control 'V-75455' do
    tag nist: ["IA-5 (01)"]
  end

  #input: min_num_characters_to_change
  control 'V-75457' do
    tag nist: ["IA-5 (01)"]
  end

  #--- end of password checking controls
end
